# Springboot Server

## Walkthrough videos
The following guides illustrate how to build a cafe data model:

* [Part 1](https://www.loom.com/share/56d5709739424bc980e9ad2a444b5bee)
* [Part 2](https://www.loom.com/share/9afbebe0baf541569cb7c46f17afeb9f)

## Deploy to AWS

### Create an AWS account

Create for yourself an AWS account (you'll have to add a credit/debit card).

### Provision EC2

Create an EC2 instance from the free tier. Just stick with the defaults. In the security section add a RULE for HTTP and select port 80.

### SSH keys

During this process you will generate a pair of keys you need to keep track of the .pem identity file as that is your only way to access your new server. Move the downloaded file somewhere safe that you can reference later. I place mine in `~/.ssh/` along side the keys I use to access Github etc. You need to restrict the permissions of that file to only-admins-can-read.

```sh
chmod 400 ./path/to/.pem
```

### Connect

Open a secure shell on your remote machine like this:

```sh
ssh -i ./path/to/you/key.pem ec2-user@x.x.x.x
```
replace the `x.x.x.x` with the ip address of your EC2 instance. You can find the public ip address of your instance on your AWS console.

### Setup

On your remote machine you'll have to install java.

```sh
sudo yum install java
```

### Upload

Exit your server. Then locally on your machine set the port to 80 and build your project.

1. Update the `application.properties` file by adding the following to the end of the file `server.port=80`. That will start your Springboot server on port 80, which is the default port for HTTP requests.
1. Now build your project `./mvnw clean install` that will generate a .jar file in your `target` folder.
1. Upload the .jar file using secure copy (scp) command below (swap out the details for your details).

```sh
scp -i ./path/to/.pem/file ./target/your-project-0.0.1-SNAPSHOT.jar ec2-user@x.x.x.x:/home/ec2-user/.
```

### Install and start

1. ssh back into your remote server i.e. `ssh -i ./path/to/you/key.pem ec2-user@x.x.x.x`
1. start your server `sudo java -jar your-project-0.0.1-SNAPSHOT.jar &`
1. that will return a PID number like 21387 to stop your process running on your server use the command below

```sh
kill 21387
```
But swap out the PID for your PID. Notice the little `&` at the end that will run your service in the background. If you now visit your server's ip address in a browser you should see your hello world page, and you should be able to hit your endpoints and interact with your service.

NOT SECURE

This project is totally insecure. Anyone can read and write to your database. Beware of this and maybe don't leave it running outside of session times.

[Video for deployment](https://www.loom.com/share/1aa230c267e44ee28696c210faabaf80)

----

# User Interfaces

## Week 4

|Mon|Tue|Wed|Thu|Fri|
|:--|:--|:--|:--|:--|
<ul><li>Use fetch to GET data from your server.</li><li>Research UI [Heuristics](https://multiverselearningproducts.github.io/curriculum/Module-1/Unit-2-Designing_UI/1.2.1-Usability_heuristics.html#javascript)</li><li>Create a design for your website</li></ul>|<ul><li>The DOM & CRUD with JS</li><li>Component lifecycle</li></ul>|<ul><li>CSS Grid & Flex recap</li><li>Component system or Atomic classes</li></ul>|<ul><li>Forms and validating user data</li><li>Finishing off projects</li></ul>|<ul><li>Forming groups and making pitches</li><li>Working collaboratively in version control</li></ul>|

### Mon

Mission: Today design your restaurant site

* Read through the [Heuristics](https://multiverselearningproducts.github.io/curriculum/Module-1/Unit-2-Designing_UI/1.2.1-Usability_heuristics.html#javascript) page.
* Start to design your site, and the interactions. Some things to consider.
    - how will you display all the restaurants?
    - will you hide some information?
    - how will you display a restaurants menus?
    - how will you display the items on an menu?
    - how would a user add another restaurant?
    - how might you add a menu to a restaurant?
    - how would you add an item to a menu?
    - if you make a mistake, how could you correct something (update) a menu or item?
    - what about deleting?
* The design work is to think through all the interactions, you can plan out your pages or components using something like powerpoint or [https://mydraft.cc/](https://mydraft.cc/).
* Here is [the video](https://www.loom.com/share/41b8f6fc2dac4f0c9a4f4c3a6037b306) of how to `fetch` data from your server for your HTML page.

### Tue

Mission: Start to build your interface

![component update cycle](https://user-images.githubusercontent.com/4499581/117293904-e8796100-ae69-11eb-839a-cb4e31b6b5f9.jpg)

* [Video of component update cycle](https://www.loom.com/share/1662c1889b304eeca26b60057ae77265)
* Get hold of your data in the browser and try to write the code that will render it.

### Wed

Mission: Start building interactions

* Have your pages and components laid out complete the READ part of CRUD
* Capture the data from the user you need to create a restaurant
* Send the data for creating a restaurant to the server.

* [Video for Grid and Flex](https://www.loom.com/share/680a5a70388d4ddf99e5e9aa115e28b9)
* [Video for POST requests from your browser](https://www.loom.com/share/30d8b3e7e53f40b28f7f12a70f36b309)

### Thurs

* Work on your project

* [Video for creating Menus and items](https://www.loom.com/share/70e37eaedaa445d385aa232d23d5456c)

### Friday

* Practice making branches and pull requests on bitbucket
* Meet with your group and plan a final project
* Pitch your idea to the rest of the group