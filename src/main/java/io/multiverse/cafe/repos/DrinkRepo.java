package io.multiverse.cafe.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import io.multiverse.cafe.Drink;

interface DrinkRepo extends JpaRepository<Drink, Integer> {
    
}
