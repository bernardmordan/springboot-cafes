package io.multiverse.cafe.repos;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.multiverse.cafe.Drink;
import io.multiverse.cafe.Menu;

@RestController
public class DrinkController {
    private MenuRepo menuRepo;
    private DrinkRepo drinkRepo;

    public DrinkController(MenuRepo menuRepo, DrinkRepo drinkRepo) {
        this.menuRepo = menuRepo;
        this.drinkRepo = drinkRepo;
    }

    @PostMapping("/cafes/{cafe_id}/menus/{menu_id}/drinks")
    public Drink addToMenu(@RequestBody Drink drinkData, @PathVariable Integer menu_id) {
        Menu menu = menuRepo.findById(menu_id).get();
        drinkData.setMenu(menu);
        return drinkRepo.save(drinkData);
    }
}
