package io.multiverse.cafe.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import io.multiverse.cafe.Cafe;

interface CafeRepo extends JpaRepository<Cafe, Integer> {
    
}
